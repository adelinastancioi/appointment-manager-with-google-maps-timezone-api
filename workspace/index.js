'use strict'
const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');

const app = express();
app.use(bodyParser.json());


const sequelize = new Sequelize('c9', 'cristinasosoi', '', {
  host: 'localhost',
   dialect: 'mysql',
   operatorsAliases: false,
   pool: {
        "max": 1,
        "min": 0,
        "idle": 20000,
        "acquire": 20000
    } ,
    
    define:{freezeTableName:true},
    
   define : {
        timestamps : false
    }
     
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });
  
  

const User=sequelize.define('user',{
  
  name :{
    
    type: Sequelize.STRING,
       allowNull: false
  },
  surname: {
        type: Sequelize.STRING,
        allowNull: false
   },
   username: {
       type: Sequelize.STRING,
       allowNull: false,
       primaryKey: true
   }, 
   password : {
       type: Sequelize.STRING,
       allowNull: false
   },
   country :{
     type: Sequelize.STRING,
        allowNull: false
   },
   phoneNumber:{
     type:Sequelize.STRING,
     allowNull :false,
      validate : { 
            isNumeric: true
        }
     },
     
     email:{
     type:Sequelize.STRING,
     allowNull :false,
     validate:{
       isEmail: true
     }
     
     }
    
});

const AppointmentList=sequelize.define('appointments',{
    type :{
      type: Sequelize.STRING,
       allowNull: false
    },
    title :{
       type: Sequelize.STRING,
       allowNull: false
    },
    date:{
        type : Sequelize.DATEONLY,
        allowNull : false,
    },
    hour :{
      type:Sequelize.TIME,
      allowNull : false
    }
    
  
});

const BusinessMeeting=sequelize.define('meetings',{
  identifier :{
    type: Sequelize.STRING, 
    primaryKey: true
  },
  title:{
    type: Sequelize.STRING,
       allowNull: false
    },
    date:{
        type : Sequelize.DATEONLY,
        allowNull : false,
    },
    hour :{
      type:Sequelize.TIME,
      allowNull : false
    },
    address:{
      type: Sequelize.STRING,
       allowNull: false
    },
    duration:{
      type:Sequelize.DOUBLE,
       allowNull: false
    },
  notes:{
    type:Sequelize.STRING
  }
    
});

const Consultation=sequelize.define('consultations',{
    identifier :{
    type: Sequelize.STRING, 
     primaryKey: true
       },
  title:{
    type: Sequelize.STRING,
       allowNull: false
    },
    date:{
        type : Sequelize.DATEONLY,
        allowNull : false,
    },
    hour :{
      type:Sequelize.TIME,
      allowNull : false
    },
    clinic :{
      type:Sequelize.STRING
     },
     clinicAddress:{
       type:Sequelize.STRING
     },
     
    doctorName:{
       type:Sequelize.STRING
    },
    
    medicalIndications:{
      type:Sequelize.STRING
    }
    
});

const Other =sequelize.define('others',{
     identifier :{
    type: Sequelize.STRING, 
     primaryKey: true
       },
      category:{
         type:Sequelize.STRING
       },
  title:{
    type: Sequelize.STRING,
       allowNull: false
    },
    date:{
        type : Sequelize.DATEONLY,
        allowNull : false,
    },
    hour :{
      type:Sequelize.TIME,
      allowNull : false
    },
    notes:{
      type:Sequelize.STRING
    }
});

AppointmentList.belongsTo(User); 

User.hasMany(BusinessMeeting);
User.hasMany(Consultation);
User.hasMany(Other);



// sequelize.sync({force: true}).then(()=>{
//     console.log('Databases create successfully')
// })





app.post('/register', (req, res) =>{
    User.create({
        name: req.body.name,
        surname: req.body.surname,
        username: req.body.username,
        password: req.body.password,
        country:req.body.country,
        phoneNumber:req.body.phoneNumber,
        email:req.body.email
    }).then((user) => {
        res.status(200).send("User created successfully");
    }, (err) => {
        res.status(500).send(err);
    })
})

app.get('/users', (req, res) => {
    User.findAll().then((users) => {
        res.status(200).send(users)
    });
});


app.post('/login', (req, res) => {
   User.findOne({where:{username: req.body.username, password: req.body.password} }).then((result) => {
       res.status(200).send(result)
   }) 
});



app.post('/add-meeting', (req, res) =>{
    BusinessMeeting.create({
        identifier:req.body.identifier,
        title:req.body.title,
        date:req.body.date,
        hour:req.body.hour,
        address:req.body.address,
        duration:req.body.duration,
        notes:req.body.notes,
        userUsername:req.body.userUsername
    }).then((meet) => {
        res.status(200).send(" The meeting has been created successfully");
    }, (err) => {
        res.status(500).send(err);
    })
})

app.post('/add-consultation', (req, res) =>{
    Consultation.create({
        identifier:req.body.identifier,
        title:req.body.title,
        date:req.body.date,
        hour:req.body.hour,
        clinic:req.body.clinic,
        clinicAddress:req.body.clinicAddress,
        doctorName:req.body.doctorName,
        medicalIndications:req.body.medicalIndications,
        userUsername:req.body.userUsername
    }).then((consultation) => {
        res.status(200).send(" The consultation has been created successfully");
    }, (err) => {
        res.status(500).send(err);
    })
})


app.post('/add-other', (req, res) =>{
    Other.create({
        identifier:req.body.identifier,
        category:req.body.category,
        title:req.body.title,
        date:req.body.date,
        hour:req.body.hour,
        notes:req.body.notes,
        userUsername: req.body.userUsername
    }).then((event) => {
        res.status(200).send(" The event has been added successfully");
    }, (err) => {
        res.status(500).send(err);
    })
})

app.get('/meetings',(req, res) => {
    BusinessMeeting.findAll().then((meetings) => {
        res.status(200).send(meetings)
    });
   
});


app.get('/consultations',(req, res)=>{
      Consultation.findAll().then((consultations)=>{
      res.status(200).send(consultations)
    });
});

app.get('/others',(req, res)=>{
     Other.findAll().then((others)=>{
     res.status(200).send(others)
    });
});




app.get('/meetings/:identifier', (req,res)=>{
    BusinessMeeting.findById(req.params.identifier).then((meeting)=>{
        if(meeting){
            res.status(200).json(meeting)
        }
        else{
            res.status(404).send('Not found')
        }
});
});


app.delete('/meetings/:identifier', (req, res) => {
   BusinessMeeting.findById(req.params.identifier).then((meeting)=>{
        if(meeting){
            meeting.destroy().then((result)=>{
                res.status(204).send();
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Server error');
            })
        }
        else{
            res.status(404).send('Resource not found');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Server error');
    });
})


app.put('/meetings/:identifier',(req,res)=>{
    BusinessMeeting.findById(req.params.identifier).then((meeting)=>{
        if(meeting){
            meeting.update(req.body).then((result)=>{
                res.status(201).json(meeting)
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Server error');
            })
        }
        else{
            res.status(404).send('Not found');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Server error');
    });
});






app.get('/consultations/:identifier', (req,res)=>{
    Consultation.findById(req.params.identifier).then((consultation)=>{
        if(consultation){
            res.status(200).json(consultation)
        }
        else{
            res.status(404).send('Not found')
        }
});
});


app.delete('/consultations/:identifier', (req, res) => {
   Consultation.findById(req.params.identifier).then((consultation)=>{
        if(consultation){
            consultation.destroy().then((result)=>{
                res.status(204).send();
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Server error');
            })
        }
        else{
            res.status(404).send('Resource not found');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Server error');
    });
})


app.put('/consultations/:identifier',(req,res)=>{
    Consultation.findById(req.params.identifier).then((consultation)=>{
        if(consultation){
            consultation.update(req.body).then((result)=>{
                res.status(201).json(consultation)
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Server error');
            })
        }
        else{
            res.status(404).send('Not found');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Server error');
    });
});









app.get('/others/:identifier', (req,res)=>{
    Other.findById(req.params.identifier).then((other)=>{
        if(other){
            res.status(200).json(other)
        }
        else{
            res.status(404).send('Not found')
        }
});
});


app.delete('/others/:identifier', (req, res) => {
   Other.findById(req.params.identifier).then((other)=>{
        if(other){
            other.destroy().then((result)=>{
                res.status(204).send();
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Server error');
            })
        }
        else{
            res.status(404).send('Resource not found');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Server error');
    });
})


app.put('/others/:identifier',(req,res)=>{
    Other.findById(req.params.identifier).then((other)=>{
        if(other){
            other.update(req.body).then((result)=>{
                res.status(201).json(other)
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Server error');
            })
        }
        else{
            res.status(404).send('Not found');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Server error');
    });
});



app.listen(8080, ()=>{
    console.log('Server started on port 8080...');
})