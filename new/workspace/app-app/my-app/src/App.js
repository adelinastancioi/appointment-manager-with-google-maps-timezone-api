import React, { Component } from 'react';

import './App.css';

import {AddConsult} from './components/Consultation/AddConsult';
import {AddEvent} from './components/Events/AddEvent';
//import {AddOther} from './components/Others/AddOther';
 import {AddMeet} from './components/Meet/AddMeet';
 import {NavMenu} from './components/NavigationMenu/NavMenu';
 import {EventList} from './components/EventList/EventList';
  import {MeetingList} from './components/MeetingsList/MeetingList';
import {ConsultationList} from './components/ConsultationList/ConsultationList';
import {UpdateMeeting} from './components/UpdateMeetings/UpdateMeeting';
import {UpdateConsultation} from './components/UpdateConsultations/UpdateConsultations';
import {Footer} from './components/Footer/Footer';
// import {OtherList} from './components/OtherList/OtherList';



class App extends Component {
  
  
   
  constructor(props){
    super(props);
    this.state = {};
    this.state.todos = [];
    this.state.meets=[];
   this.state.consults=[];
   this.state.others=[];
    
  }
  
  onItemAdded = (event) => {
    this.state.todos.push(event);
    let todos = this.state.todos;
    this.setState({
      todos: todos
    })
    console.log(this.state.todos)
  }
  
  onItemAdded2=(meet)=>{
    this.state.meets.push(meet);
    let meets=this.state.meets;
     this.setState({
      meets: meets
    })
    console.log(this.state.meets)
  }
  
  
  onItemAdded3=(consult)=>{
    this.state.consults.push(consult);
    let consults=this.state.consults;
     this.setState({
      consults: consults
    })
    console.log(this.state.consults)
  }
  
   onItemAdded4=(other)=>{
    this.state.others.push(other);
    let others=this.state.others;
     this.setState({
      others: others
    })
    console.log(this.state.others)
  }
  
  
  componentWillMount(){
    const url = 'https://new-cristinasosoi.c9users.io/events'
    fetch(url).then((res) => {
      return res.json();
    }).then((todos) =>{
      this.setState({
        todos: todos
      })
    })
  }
  
  componentDidMount(){
        Promise.all([
            fetch('https://new-cristinasosoi.c9users.io/events'),
            fetch('https://new-cristinasosoi.c9users.io/meetings'),
            fetch('https://new-cristinasosoi.c9users.io/consultations'),
            fetch('https://new-cristinasosoi.c9users.io/others'),
            
        ])
        .then(([res1, res2,res3,res4]) => Promise.all([res1.json(), res2.json(),res3.json(),res4.json()]))
        .then(([data1, data2,data3,data4]) => this.setState({
            todos: data1, 
            meets: data2,
            consults:data3,
            others:data4
        }));
    }
  
  
  
  
  render() {
    
     let links = [
     
      { label: 'Create Appointment', link: '#about' },
      { label: 'Update Appointments', link: '#updates' },
      { label: 'Contact Us', link: '#contactus' },
    ];
    
    
    return(
      
      <React.Fragment>
    <div className="container center">
        <NavMenu links={links} />
      </div>
      <h1> Appointment Manager </h1>
      <div className="grid-container">
              <div className="item" id="addEventItem">
               <AddEvent  handleAdd={this.onItemAdded} />
               </div>
               <div className="item" id="addMeetItem">
               <AddMeet handleAdd={this.onItemAdded2} />
               </div>
               <div className="item" id="addConsultItem">
               <AddConsult  handleAdd={this.onItemAdded3} />
               </div>
               
               
      </div>

        <div className="grid-container-display">
        <div className="item" id="eventListItem">
        <EventList title="EventList" source={this.state.todos} />
        </div>
       
        <div className="item" id="meetingListItem">
        <MeetingList title="Business Meetings" source={this.state.meets} />
        </div>
        <div className="item" id="consultationListItem">
        <ConsultationList title="Medical Checklist" source={this.state.consults} />
        </div>
        <div className = "item" id="updates">
        <UpdateMeeting title = "Update Meeting" source={this.state.meets}/>
        </div>
        
        <div className = "item" id = "updateConsultationItem">
        <UpdateConsultation title = "Update Consultation" source={this.state.consults}/>
        </div>
        
         <div className = "item" id="contactus">
        <Footer />
        </div>
        
        
        
        </div>
      
       

       </React.Fragment>
      );
  }
}

export default App;


// { label: 'Home', link: '#home', active: true },
