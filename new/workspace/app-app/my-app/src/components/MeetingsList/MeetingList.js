import React from 'react';
import axios from 'axios';


export class MeetingList extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.title = "";
        this.state.date = "";
        this.state.hour="";
        this.state.address = "";
        this.state.duration=0;
        this.state.notes="";
    }
    

     handleChangeTitle = (meet) => {
        this.setState({
            title: meet.target.value
        })
    }
    
    handleChangeDate = (meet) => {
        this.setState({
            date: meet.target.value
        })
    }
    
    handleChangeHour = (meet) => {
        this.setState({
            hour: meet.target.value
        })
    }
    
     handleChangeAddress = (meet) => {
        this.setState({
            address: meet.target.value
        })
    }
    
    handleChangeDuration = (meet) => {
        this.setState({
            duration: meet.target.value
        })
    }
    
    handleChangeNotes = (meet) => {
        this.setState({
            notes: meet.target.value
        })
    }    


    render(){
        let items = this.props.source.map((meet) =>{
            return <tr> 
            <td key={meet.identifier+meet.title}>{meet.identifier}</td>
            <td key={meet.identifier+meet.title}>{meet.title}</td>
            <td key={meet.identifier+meet.title}>{meet.date}</td>
            <td key={meet.identifier+meet.title}>{meet.hour}</td>
            <td key={meet.identifier+meet.title}>{meet.address}</td>
            <td key={meet.identifier+meet.title}>{meet.duration}</td>
            <td key={meet.identifier+meet.title}>{meet.notes}</td>
            <td>
            <button id="delete"
            onClick={() => {
            console.log("props",meet);
            axios.delete('https://new-cristinasosoi.c9users.io/meetings/'+meet.identifier)
            .then(res => {
                console.log(res);
                console.log(res.data);
                window.location.reload();
            })
            }}
            >Delete</button>
         
            
                  
          </td>
            
            </tr>
          
         
        })
        return(
            <div>
            <div>Meeting List</div>
            <table>
        <thead> 
        <td>Id</td>
        <td>Title</td>
        <td>Date</td>
        <td>Hour</td>
        <td>Address</td>
        <td>Duration</td>
        <td>Notes</td>
        <td>Actions</td>
      
        </thead>
         
        <tbody>
         {items}
        </tbody>
      </table>
      </div>
            );
    }
    
    
    //  render() {
    //         let items = this.props.source.map((todo) =>{
    //         return <tr><td key={todo.identifier}>{todo.identifier}</td>
    //         <td key={todo.identifier}>{todo.title}</td>
    //         <td key={todo.identifier}>{todo.type}</td>
    //         <td key={todo.identifier}>{todo.date}</td>
    //         <td key={todo.identifier}>{todo.hour}</td>
    //         </tr>
    //         // <td><div><button 
    //         // onClick={() => {
    //         // console.log("props",todo);
    //         // axios.delete('https://seminar-cristinasosoi.c9users.io/meetings/'+todo.identifier)
    //         // .then(res => {
    //         //     console.log(res);
    //         //     console.log(res.data);
    //         // })
    //         // }}
    //         // >Update</button></div><div><button id = "#delete">Delete</button></div></td>
            
    //     })
    // return (
    // <div><h1>Event List</h1>    
    //   <table>
    //     <thead> 
    //     <td>Id</td>
    //     <td>Title</td>
    //     <td>Type</td>
    //     <td>Date</td>
    //     <td>Hour</td>
      
    //     </thead>
         
    //     <tbody>
    //      {items}
    //     </tbody>
    //   </table>
    //   </div>
    // );
    //       }
}