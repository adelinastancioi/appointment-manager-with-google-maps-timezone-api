import React from 'react';
import axios from 'axios';

export class AddConsult extends React.Component {
 constructor(props){
        super(props);
        this.state = {};
        this.state.title = "";
        this.state.date = "";
        this.state.hour="";
        this.state.clinic = "";
        this.state.clinicAddress="";
        this.state.doctorName="";
        this.state.medicalIndication="";
    }
 
 
     handleChangeTitle = (consult) => {
        this.setState({
            title: consult.target.value
        })
    }
    
    handleChangeDate = (consult) => {
        this.setState({
            date: consult.target.value
        })
    }
    
    handleChangeHour = (consult) => {
        this.setState({
            hour: consult.target.value
        })
    }
     handleChangeClinic= (consult) => {
        this.setState({
            clinic: consult.target.value
        })
    }
     handleChangeClinicAddress = (consult) => {
        this.setState({
            clinicAddress: consult.target.value
        })
    }
    handleChangeDoctorName = (consult) => {
        this.setState({
            doctorName: consult.target.value
        })
    }
   
    
    handleChangeMedicalIndication = (consult) => {
        this.setState({
            medicalIndication: consult.target.value
        })
    }
    
    
    
    
    
    handleAddClick = () => {
        let consult = {
            
            title: this.state.title,
            date: this.state.date,
            hour:this.state.hour,
            clinic:this.state.duration,
            clinicAddress:this.state.clinicAddress,
            doctorName:this.state.doctorName,
            medicalIndication:this.state.doctorName
           
        }
        
        
        axios.post('https://new-cristinasosoi.c9users.io/add-consultation', consult).then((res) => {
            if(res.status === 200){
                this.props.consultAdded(consult)
                
            }
        }).catch((err) =>{
            console.log(err)
        })
        // window.location.reload();
    }
    
  render(){
        return(
            <div>
                <div id="addConsultSubtitle">Add Medical Consult</div>
                <input type="text" placeholder="Title" 
                    onChange={this.handleChangeTitle}
                    value={this.state.title} />
                 <input type="date" 
                    value={this.state.date}
                    onChange={this.handleChangeDate} />
                    <input type="time" value={this.state.hour}
                    onChange={this.handleChangeHour} />
                    
                    <input type="text" placeholder="ClinicName"  value={this.state.clinic}
                    onChange={this.handleChangeClinic} />
                     <input type="text" placeholder="ClinicAddress" value={this.state.clinicAddress}
                    onChange={this.handleChangeClinicAddress} />
                    
                   <input type="text" placeholder="DoctorName"  value={this.state.doctorName}
                    onChange={this.handleChangeDoctorName} />
                    <input type="text" placeholder="MedicalIndication"  value={this.state.medicalIndication}
                    onChange={this.handleChangeMedicalIndication} />
                    
                    
                <button onClick={this.handleAddClick}>Add Consult </button>
            </div>
            );
    }
 
 
 

}