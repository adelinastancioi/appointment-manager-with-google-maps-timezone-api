import React from 'react';
import axios from 'axios';

export class AddMeet extends React.Component {
 constructor(props){
        super(props);
        this.state = {};
        this.state.title = "";
        this.state.date = "";
        this.state.hour="";
        this.state.address = "";
        this.state.duration=0;
        this.state.notes="";
    }
 
 
     handleChangeTitle = (meet) => {
        this.setState({
            title: meet.target.value
        })
    }
    
    handleChangeDate = (meet) => {
        this.setState({
            date: meet.target.value
        })
    }
    
    handleChangeHour = (meet) => {
        this.setState({
            hour: meet.target.value
        })
    }
    
     handleChangeAddress = (meet) => {
        this.setState({
            address: meet.target.value
        })
    }
    
    handleChangeDuration = (meet) => {
        this.setState({
            duration: meet.target.value
        })
    }
    
    handleChangeNotes = (meet) => {
        this.setState({
            notes: meet.target.value
        })
    }
    
    
    
    
    
    handleAddClick = () => {
        let meet = {
            
            title: this.state.title,
            date: this.state.date,
            hour:this.state.hour,
            address:this.state.address,
            duration:this.state.duration,
            notes:this.state.notes
            
        }
        
        
        axios.post('https://new-cristinasosoi.c9users.io/add-meeting', meet).then((res) => {
            if(res.status === 200){
                this.props.meetAdded(meet)
            }
        }).catch((err) =>{
            console.log(err)
        })
        
        //window.location.reload();
    }
    
  render(){
        return(
            <div>
                <div id="addMeetingSubtitle">Add Meeting</div>
                <input type="text" placeholder="Title" 
                    onChange={this.handleChangeTitle}
                    value={this.state.title} />
                   
                     
                 <input id="dt" type="date" 
                    value={this.state.date}
                    onChange={this.handleChangeDate} /> <br/>
                   
                    <label htmlFor="tm">Time</label>
                    <input id="tm" type="time" value={this.state.hour}
                    onChange={this.handleChangeHour} />
                    <input type="text"  placeholder="Address" value={this.state.address}
                    onChange={this.handleChangeAddress} /><br/>
                     <label htmlFor="dr">Duration</label>
                    <input id="dr" type="number" placeholder="Duration" value={this.state.duration}
                    onChange={this.handleChangeDuration} />
                    <input type="text" placeholder="Notes" value={this.state.notes}
                    onChange={this.handleChangeNotes} />
                    
                <button onClick={this.handleAddClick}>Add BussinesMeeting </button>
            </div>
            );
    }
 
 
 

}