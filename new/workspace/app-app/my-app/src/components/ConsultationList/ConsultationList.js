import React from 'react';
import axios from 'axios';

export class ConsultationList extends React.Component {
    // constructor(props){
    //     super(props);
    //      }
    
    render(){
        let items = this.props.source.map((consult) =>{
            return <tr> 
            <td key={consult.identifier+consult.title}>{consult.identifier}</td>
            <td key={consult.identifier+consult.title}>{consult.title}</td>
            <td key={consult.identifier+consult.title}>{consult.date}</td>
            <td key={consult.identifier+consult.title}>{consult.hour}</td>
            <td key={consult.identifier+consult.title}>{consult.clinic}</td>
            <td key={consult.identifier+consult.title}>{consult.clinicAddress}</td>
            <td key={consult.identifier+consult.title}>{consult.doctorName}</td>
            <td key={consult.identifier+consult.title}>{consult.medicalIndication}</td>
            <td>
            <button id="delete"
            onClick={() => {
            console.log("props",consult);
            axios.delete('https://new-cristinasosoi.c9users.io/consultations/'+consult.identifier)
            .then(res => {
                console.log(res);
                console.log(res.data);
                window.location.reload();
            })
            
            }}
            >Delete</button></td>
            </tr>
        })
        return(
             <div>
            <div>Medical Checklist</div>
            <table>
        <thead> 
        <td>Id</td>
        <td>Title</td>
        <td>Date</td>
        <td>Hour</td>
        <td>Clinic</td>
        <td>Address</td>
        <td>Doctor's name</td>
        <td>Medical Indications</td>
        <td>Actions</td>
      
        </thead>
         
        <tbody>
         {items}
        </tbody>
      </table>
      </div>
            );
    }
    
   
}