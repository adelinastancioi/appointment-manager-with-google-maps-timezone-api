import React from 'react';
import axios from 'axios';

export class AddEvent extends React.Component {
 constructor(props){
        super(props);
        this.state = {};
        this.state.type = ""
        this.state.title = ""
        this.state.date = " "
        this.state.hour="";
    }
 
 
      handleChangeType = (event) => {
        this.setState({
            type: event.target.value
        })
    }
    
    handleChangeTitle = (event) => {
        this.setState({
            title: event.target.value
        })
    }
    
    handleChangeDate = (event) => {
        this.setState({
            date: event.target.value
        })
    }
    
     handleChangeHour = (event) => {
        this.setState({
            hour: event.target.value
        })
    }
    
    
    
    
    handleAddClick = () => {
        let event = {
            type: this.state.type,
            title: this.state.title,
            date: this.state.date,
            hour:this.state.hour
        }
        
        
        axios.post('https://new-cristinasosoi.c9users.io/add-event', event).then((res) => {
            if(res.status === 200){
                this.props.todoAdded(event)
            }
        }).catch((err) =>{
            console.log(err)
        })
        
       // window.location.reload();
    }
    
  render(){
        return(
            <div>
                <div id="addEventSubtitle" >Add Event</div>
                <input id="idInput" type="text" placeholder="Type" 
                    onChange={this.handleChangeType}
                    value={this.state.type} />
                <input type="text" placeholder="Title" 
                    onChange={this.handleChangeTitle}
                    value={this.state.title} />
                <input type="date" value={this.state.date}
                    onChange={this.handleChangeDate} />
                    <input type="time" value={this.state.hour}
                    onChange={this.handleChangeHour} />
                <button onClick={this.handleAddClick}>Add Event </button>
            </div>
            );
    }
 
 
 

}