import React from 'react';
import { Grid, Nav, NavItem } from 'react-bootstrap';

export function Footer(/*props*/) {
  return (
    <footer>
      <Grid>
        <Nav justified>
          <NavItem 
            eventKey={1}>
            Privacy policy
          </NavItem>
          <NavItem
            eventKey={2}
            title="Item">
            Terms & Conditions
          </NavItem>
        
        </Nav>

        <div className="text-center small copyright">
          ©  2016
        </div>
      </Grid>
    </footer>
  );
}

export default Footer;